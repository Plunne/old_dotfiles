vim.o.termguicolors = true

-- colors for active , inactive buffer tabs 
require "bufferline".setup {
    options = {
        buffer_close_icon = "",
        modified_icon = "●",
        close_icon = "",
        left_trunc_marker = "",
        right_trunc_marker = "",
        max_name_length = 14,
        max_prefix_length = 13,
        tab_size = 18,
        enforce_regular_tabs = true,
        view = "multiwindow",
        show_buffer_close_icons = true,
        separator_style = {"", ""}
    },
    highlights = {
        background = {
            guifg = comment_fg,
            guibg = "#0B0C15"
        },
        fill = {
            guifg = comment_fg,
            guibg = "#00000000"
        },
        buffer_selected = {
            guifg = normal_fg,
            guibg = "#00000000",
            gui = "bold"
        },
        separator_visible = {
            guifg = "#00000000",
            guibg = "#00000000"
        },
        separator_selected = {
            guifg = "#00000000",
            guibg = "#00000000"
        },
        separator = {
            guifg = "#00000000",
            guibg = "#00000000"
        },
        indicator_selected = {
            guifg = "#FF87FF",
            guibg = "#00000000"
        },
        modified = {
            guifg = string_fg,
            guibg = "#0B0C15"
        },
        modified_visible = {
            guifg = string_fg,
            guibg = "#0B0C15"
        },
        modified_selected = {
            guifg = string_fg,
            guibg = "#00000000"
        }
    }
}

local opt = {silent = true}

vim.g.mapleader = " "

--command that adds new buffer and moves to it
vim.api.nvim_command "com -nargs=? -complete=file_in_path New badd <args> | blast"
vim.api.nvim_set_keymap("n","<S-b>",":New ", opt)

--removing a buffer
vim.api.nvim_set_keymap("n","<S-f>",[[<Cmd>bdelete<CR>]], opt)

-- tabnew and tabprev
vim.api.nvim_set_keymap("n", "<S-l>", [[<Cmd>BufferLineCycleNext<CR>]], opt)
vim.api.nvim_set_keymap("n", "<S-s>", [[<Cmd>BufferLineCyclePrev<CR>]], opt)
