#!/bin/sh

apps_dir() {
    cd ~/.apps
}

########################
#
# Applications
#
########################

install_nvim() {
    sudo pacman -S base-devel cmake unzip ninja tree-sitter curl
    git clone https://gitlab.com/Plunne/neovim.git
    cd neovim
    sudo make install
    apps_dir
}

install_st() {
    git clone https://gitlab.com/Plunne/st.git
    cd st
    sudo make install clean
    apps_dir
}

########################
#
# Install
#
########################

install_nvim
install_st
