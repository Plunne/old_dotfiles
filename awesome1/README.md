# PLUNNE AWESOME

<br/>

There are my AwesomeWM dotfiles. (W.I.P.)

<br/>

Softwares I use
---------------

- **Terminal :** St
- **Editor :** Neovim
- **Filemanager :** Ranger & Thunar
- **Web browser :** Firefox
- **PDF Reader :** Zathura
- **Screenshots :** Flameshot

Environment
-----------

- **WM :** AwesomeWM
- **Bar :**
- **Keybinds :**
- **Composer :** Picom (Jonaburg fork)
- **Launcher :** Rofi
- **Notifications :**
- **Wallpaper :**

<br/>

Features
--------

There are some features you will have with my config.

Installation
------------

1. Clone this repo and go to `plunne_dotfiles/awesomewm` directory.

    ```shell
    git clone https://gitlab.com/Plunne/plunne_dotfiles
    cd plunne_dotfiles/awesomewm
    ```

2. Check the `Makefile` and `install.sh` and change some apps if you want. 

3. Run the shell script install.sh
    ```shell
    ./install.sh
    ```

4. The system will reboot automatically

5. Enjoy!

<br/>

Thanks to
---------

**siduck76 (for awesome nvim tips) :** https://github.com/siduck76/neovim-dotfiles <br/>
**adi1090x (for awesome rofi configs) :** https://github.com/adi1090x/rofi <br/>

more is coming...

Thanks you for watching/using my dots, have nice moments with my config! <3
