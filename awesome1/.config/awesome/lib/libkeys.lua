local awful = require("awful")
local gears = require("gears")
local hotkeys_popup = require("awful.hotkeys_popup")
local menubar = require("menubar")

require("config.vars")

--[[--------------------------------------------------------
        KEYS
--]]--------------------------------------------------------

-- Keys Combinaisons
super   = {modkey,}
alt     = {altkey,}
ctrl    = {"Control",}
shift   = {"Shift",}

super_alt     = {modkey, altkey}
super_ctrl    = {modkey, "Control"}
super_shift   = {modkey, "Shift"}

super_alt_ctrl    = {modkey, altkey, "Control"}
super_alt_shift   = {modkey, altkey, "Shift"}

super_ctrl_shift  = {modkey, "Control", "Shift"}

--[[--------------------------------------------------------
        FUNCTIONS
--]]--------------------------------------------------------

-- Bind functions
function bind(mod, key, g, d, action)       
    return awful.key( mod, key, action, { description=d, group=g })
end

function bindApp(mod, key, app, g, d)      
    return awful.key( mod, key, function() awful.spawn(app) end, { description=d, group=g })
end

-- Click functions
function click(click, action)       
    return awful.button( {}, click, action)
end

function clickMod(mod, click, action)       
    return awful.button( mod, click, action)
end