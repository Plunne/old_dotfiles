awful = require("awful")
beautiful = require("beautiful")

l = awful.layout.suit

--[[--------------------------------------------------------
        LAYOUTS
--]]--------------------------------------------------------

function set_layouts(args)
    awful.layout.layouts = args
end

--[[--------------------------------------------------------
        DESKTOPS
--]]--------------------------------------------------------

function add_desktop(name, s, l, ratio, gap, sel, icon)
        
    awful.tag.add(
        name,
        {
            screen              = s,
            layout              = l,
            master_width_factor = ratio,
            gap_single_client   = gap,
            selected            = sel,
            icon                = icon,
        }
    )

end

--[[--------------------------------------------------------
        RULES
--]]--------------------------------------------------------

function set_rules(rules)
    awful.rules.rules = rules
end

function rules_All()
    return {
        rule = { },
        properties = {
            border_width = beautiful.border_width,
            border_color = beautiful.border_normal,
            focus = awful.client.focus.filter,
            raise = true,
            keys = require("keys.keyboard").clientkeys(),
            buttons = require("keys.mouse").clientbuttons(),
            screen = awful.screen.preferred,
            placement = awful.placement.no_overlap+awful.placement.no_offscreen
        }
    }
end

function rules_Titlebars(bool)
    return { rule_any = { type = { "normal", "dialog" } }, properties = { titlebars_enabled = bool } }
end

function rules_Single(app, props)
    return { rule = { instance = app }, properties = props }
end

function rules_Multiple(apps, props)
    return { rule_any = { instance = apps }, properties = props }
end

function rules_Single_Except(app, props)
    return { rule = { }, except = { instance = app }, properties = props }
end

function rules_Multiple_Excepts(apps, props)
    return { rule = { }, except_any = { instance = apps }, properties = props }
end

--[[--------------------------------------------------------
        AUTOSTART
--]]--------------------------------------------------------

-- Deamons
function run_deamons(deamons)

    for app = 1, #deamons do
        awful.spawn.single_instance(deamons[app], awful.rules.rules)
    end
end

-- Apps
function run(myapp, mytag)
    awful.spawn( myapp, { screen = awful.screen.focused(), tag = mytag } ) 
end