local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")
local wibox = require("wibox")
--local xprop = require("xproperties")

local b = awful.titlebar.widget

require("lib.libkeys")

--[[--------------------------------------------------------
        TITLEBAR
--]]--------------------------------------------------------

client.connect_signal("request::titlebars", function(c)

    local window_title = { 
        align  = "center",
        widget = b.titlewidget(c)
    }
    local move_window = click(left_click, function() c:emit_signal("request::activate", "titlebar", {raise = true}) awful.mouse.client.move(c) end )
    
    local bottom_line = wibox.container.margin()
    bottom_line.bottom = 1
    
    --BEGIN Compatible with v4.3
    client.connect_signal("focus", function(c2)
        if c == c2 then
            bottom_line.color = beautiful.titlebar_fg_focus
        end
    end)
    client.connect_signal("unfocus", function(c2)
        if c == c2 then
            bottom_line.color = beautiful.titlebar_fg_normal
        end
    end)
    --END compat with v4.3
    
    -- GIT MASTER VERSION
    --c:connect_signal("property::active", function()
    --    margin_widget.color = c.active and beautiful.color01 or beautiful.color_dark
    --end)

    bottom_line.widget = wibox.widget {
        
        wibox.widget {
            --------------------
            --      Left      --
            --------------------
            {   b.ontopbutton(c),
                layout  = wibox.layout.fixed.horizontal
            },
            --------------------
            --     Middle     --
            --------------------
            {   window_title,
                buttons = move_window,
                layout  = wibox.layout.flex.horizontal
            },
            --------------------
            --      Right     --
            --------------------
            {   b.minimizebutton(c), b.floatingbutton(c), b.closebutton(c),
                layout = wibox.layout.fixed.horizontal
            },

            layout = wibox.layout.align.horizontal
        },
        widget = wibox.container.margin(self, beautiful.titlebar_hmargin, beautiful.titlebar_hmargin, beautiful.titlebar_vmargin, beautiful.titlebar_vmargin)
    }

    awful.titlebar(c, {size=beautiful.titlebar_size}).widget = bottom_line

end)