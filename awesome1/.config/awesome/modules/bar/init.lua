local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")
local wibox = require("wibox")


--[[--------------------------------------------------------
        BAR
--]]--------------------------------------------------------

local function bar(s)

    require("system.wallpaper")(s)

    awful.screen.connect_for_each_screen(function(s)
        
        -- Wallpaper
        set_wallpaper(s)
        
        -- Modules
        s.mypromptbox = require("modules.bar.promptbox")(s)
        s.mylayoutbox = require("modules.bar.layoutbox")(s)
        s.mydesktops = require("modules.bar.taglist")(s)
        s.mytasklist = require("modules.bar.tasklist")(s)
        s.mysystray = require("modules.bar.systray")(s)
        s.mytextclock = require("modules.bar.datetime")(s)
        s.mylauncher = require("modules.bar.launcher")(s)
        s.spacer1 = require("modules.bar.spacer1")(s)

        -------------------
        --     PANEL     --
        -------------------

        -- Create the wibox
        s.mywibox = awful.wibar{
            screen = s,
            position = "top",
            height = 32,
            bg = beautiful.bg_minimize
        }

        -- Add widgets to the wibox
        s.mywibox:setup {
            layout = wibox.layout.align.horizontal,
            
            -------------------------
            --     Left widgets    --
            -------------------------
            {   layout = wibox.layout.fixed.horizontal,
                s.mylayoutbox,
                s.mydesktops,
                s.mypromptbox
            },
            -------------------------
            --    Middle widgets   --
            -------------------------   
            {
                layout = wibox.layout.flex.horizontal,   
                s.mytasklist,
            },
            -------------------------
            --    Right widgets    --
            -------------------------
            {   layout = wibox.layout.fixed.horizontal,
                s.spacer1,
                s.mysystray,
                s.spacer1,
                s.mytextclock,
                s.mylauncher
            },
        }
    end)
    
end

return bar
