local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")
local wibox = require("wibox")

-- Cairo
local cairo = require("lgi").cairo
local img = cairo.ImageSurface.create(cairo.Format.ARGB32, 50, 50) -- Create a surface
local cr  = cairo.Context(img) -- Create a context

require("modules.general.menu")

--[[--------------------------------------------------------
        LAUNCHER
--]]--------------------------------------------------------

local function create(s)
    
    local launcher = wibox.widget {

        wibox.widget {

            awful.widget.launcher {
                screen  = s,
                image   = beautiful.awesome_icon,
                menu    = mymainmenu,
            },

            shape  = function(cr, w, h) return gears.shape.rounded_rect(cr, w, h, 4) end,
            shape_clip = true,
            shape_border_color = beautiful.bg_minimize,

            widget = wibox.container.background
        },

        widget = wibox.container.margin(self, 6, 10, 6, 6)
    }
    
    return launcher
end

return create