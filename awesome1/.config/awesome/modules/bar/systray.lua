local wibox = require("wibox")

--[[--------------------------------------------------------
        SYSTRAY
--]]--------------------------------------------------------

local function create(s)
    
    local systray = wibox.widget.systray()
    
    return systray
end

return create