local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")

--[[--------------------------------------------------------
        LAYOUTBOX
--]]--------------------------------------------------------

local function create(s)
    
    local layoutbox = awful.widget.layoutbox(s)
    layoutbox:buttons(gears.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),	-- Next layout mode (left click)
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),	-- Previous layout mode (right click)
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),	-- Next layout mode (scroll up)
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))	-- Previous layout mode (scroll down)

    layouts = wibox.widget {
        
        wibox.widget {
            layoutbox,
            widget = wibox.container.background
        },

        widget = wibox.container.margin(self, 10, 6, 6, 6)
    }
    
    return layouts
end

return create