local awful = require("awful")

--[[--------------------------------------------------------
        PROMPTBOX
--]]--------------------------------------------------------

local function create(s)
    
    local promptbox = awful.widget.prompt()
    
    return promptbox
end

return create