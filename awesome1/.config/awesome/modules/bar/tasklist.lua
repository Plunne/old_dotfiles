local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")
local wibox = require("wibox")
local tasklist_buttons = require("keys.mouse").tasklist_mouse()

--[[--------------------------------------------------------
        TASKLIST
--]]--------------------------------------------------------

local function create(s)

    local tasklist = awful.widget.tasklist {
        screen   = s,
        filter   = awful.widget.tasklist.filter.currenttags,
        buttons  = tasklist_buttons,
        style    = {
            shape_border_width = 4,
            shape_border_color = beautiful.color_dark,
            shape  = function(cr, w, h) return gears.shape.rounded_rect(cr, w, h, 8) end,
        },
        layout = {
            spacing = 0,
            max_widget_size = 250,
            layout  = wibox.layout.flex.horizontal
        },
        -- Notice that there is *NO* wibox.wibox prefix, it is a template,
        -- not a widget instance.
        widget_template = {
            {
                {
                    {
                        {{ id = 'icon_role', widget = wibox.widget.imagebox }, margins = 4, widget = wibox.container.margin},
                        { id = 'text_role', widget = wibox.widget.textbox },
                        layout = wibox.layout.fixed.horizontal,
                    },
                    valign = 'center',
                    halign = 'center',
                    widget = wibox.container.place,
                },
                top = 2,
                bottom = 2,
                left  = 8,
                right = 16,
                widget = wibox.container.margin,
            },
            id     = 'background_role',
            widget = wibox.container.background,
        },
    }

    return tasklist
end

return create