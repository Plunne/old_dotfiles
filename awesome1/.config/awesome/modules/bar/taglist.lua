local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")
local taglist_buttons = require("keys.mouse").taglist_mouse()

--[[--------------------------------------------------------
        TAGLIST
--]]--------------------------------------------------------

local function create(s)

    local desktops = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.noempty,
        buttons = taglist_buttons,
        style   = {
            shape_border_width = 4,
            shape_border_color = beautiful.color_dark,
            shape  = function(cr, w, h) return gears.shape.rounded_rect(cr, w, h, 8) end,
        }
    }

    return desktops
end

return create