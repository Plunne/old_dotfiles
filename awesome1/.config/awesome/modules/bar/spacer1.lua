local wibox = require("wibox")

--[[--------------------------------------------------------
        SPACER #1
--]]--------------------------------------------------------

local function create(s)
    
    local spacer1 = wibox.widget {
        markup = ' ',
        align  = 'center',
        valign = 'center',
        widget = wibox.widget.textbox
    }
    
    return spacer1
end

return create