-- Apps
terminal = "st"
editor = "nvim"
editor_cmd = terminal .. " -e " .. editor

-- Keys Aliases
modkey  = "Mod4"
altkey  = "Mod1"
enter   = "Return"
space   = "space"
esc     = "Escape"
tab     = "Tab"

-- Arrow keys
up      = "Up"
down    = "Down"
left    = "Left"
right   = "Right"

-- Click Aliases
left_click  = 1
right_click = 3
scroll_up   = 4
scroll_down = 5
