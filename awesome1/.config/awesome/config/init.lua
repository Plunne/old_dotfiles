require("lib.libconfig")

--[[---------------------------------------------------------------------   
    LAYOUTS
    Uncomment the layouts you want to use
--]]---------------------------------------------------------------------

set_layouts({
    --l.floating,
    l.tile,
    --l.tile.left,
    --l.tile.bottom,
    --l.tile.top,
    --l.fair,
    --l.fair.horizontal,
    --l.spiral,
    --l.spiral.dwindle,
    l.max,
    --l.max.fullscreen,
    --l.magnifier,
    --l.corner.nw,
    --l.corner.ne,
    --l.corner.sw,
    --l.corner.se,
})

--[[---------------------------------------------------------------------  
    DESKTOPS
    add_desktop(name, screen, layout, ratio, gap, selected, icon_path)
--]]---------------------------------------------------------------------

add_desktop(" 1/ ",    s,  l.tile, 0.6,    false,  true,   nil)
add_desktop(" 2/ ",    s,  l.max,  0.7,    false,  false,  nil)
add_desktop(" 3/ ",    s,  l.tile, 0.6,    false,  false,  nil)
add_desktop(" 4 ",      s,  l.tile, 0.6,    false,  false,  nil)
add_desktop(" 5 ",      s,  l.tile, 0.6,    false,  false,  nil)
add_desktop(" 6 ",      s,  l.tile, 0.6,    false,  false,  nil)
add_desktop(" 7 ",      s,  l.tile, 0.6,    false,  false,  nil)
add_desktop(" 8/阮 ",   s,  l.tile, 0.6,    false,  false,  nil)
add_desktop(" 9/ ",    s,  l.tile, 0.6,    false,  false,  nil)

--[[---------------------------------------------------------------------   
    RULES
    rules_Single(app, props), rules_Multiple(apps, props)
    rules_Types(t, props)
    rules_Single_Except(app, props), rules_Multiple_Excepts(apps, props)
--]]---------------------------------------------------------------------

set_rules({
    
    rules_All(),
    rules_Titlebars(true),
    rules_Single("arandr", { floating = true }),
    rules_Multiple({"Navigator", "codium"}, { titlebars_enabled = false })

})
--[[---------------------------------------------------------------------
    AUTOSTART
    run_deamons({deamon1, deamons2})
    run(myapp, mytag)
--]]---------------------------------------------------------------------
run_deamons({"picom", "xrdb .Xressources"})
run("firefox", " 2/ ")
