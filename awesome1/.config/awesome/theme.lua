local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi
local gfs = require("gears.filesystem")

-- paths
local default_path = gfs.get_themes_dir()
local wall_path = os.getenv("HOME").."/.wallpapers/"
local theme_path = os.getenv("HOME").."/.config/awesome/theme/"

local theme = {}

-- Wallpaper
theme.wallpaper = wall_path.."rainbow-dash-my-little-pony-friendship-is-magic-cloudsdale-3940x2160-4558.png"

-- Fonts
theme.font          = "JetBrainsMono NerdFont 12"
theme.titlebar_font = "JetBrainsMono NerdFont 10"
theme.taglist_font  = "JetBrainsMono NerdFont 12"
theme.tasklist_font = "JetBrainsMono NerdFont 10"

-- Config
theme.useless_gap   = dpi(4)
theme.border_width  = dpi(0)
theme.maximized_hide_border = true


-- Colors
theme.color01       = "#6c65c0"
theme.color02       = "#413c73"
theme.color03       = "#2e2b52"
theme.color_dark    = "#211f3b"
theme.color_light   = "#ffffff"
theme.color_urgent  = "#c56259"

theme.bg_normal     = theme.color03
theme.bg_focus      = theme.color01
theme.bg_minimize   = theme.color_dark
theme.bg_urgent     = theme.color_urgent
theme.bg_systray    = theme.color03

theme.fg_normal     = theme.color01
theme.fg_focus      = theme.color_light
theme.fg_urgent     = theme.color_light
theme.fg_empty      = theme.color02
theme.fg_minimize   = theme.color01

theme.border_normal = theme.color_dark
theme.border_focus  = theme.color01
theme.border_marked = theme.color_urgent

-- taglist_[bg|fg]_[focus|urgent|occupied|empty|volatile]
theme.taglist_fg_focus      = theme.color_light
theme.taglist_bg_focus      = theme.color01
theme.taglist_fg_occupied   = theme.color01
theme.taglist_bg_occupied   = theme.color_dark
theme.taglist_fg_urgent     = theme.color01
theme.taglist_bg_urgent     = theme.color_dark

-- tasklist_[bg|fg]_[focus|urgent]
theme.tasklist_fg_normal    = theme.color01
theme.tasklist_bg_normal    = theme.color03
theme.tasklist_fg_minimize  = theme.color01
theme.tasklist_bg_minimize  = theme.color_dark
theme.tasklist_fg_urgent    = theme.color_light
theme.tasklist_bg_urgent    = theme.color_dark

-- titlebar_[bg|fg]_[normal|focus]
theme.titlebar_size         = dpi(32)
theme.titlebar_vmargin      = 2
theme.titlebar_hmargin      = 4
theme.titlebar_fg_focus     = theme.color01
theme.titlebar_bg_focus     = theme.color_dark
theme.titlebar_fg_normal    = theme.color03
theme.titlebar_bg_normal    = theme.color_dark

-- menu_[bg|fg]_[normal|focus] [border_color|border_width]
theme.menu_submenu_icon = theme_path.."submenu.png"
theme.menu_height = dpi(32)
theme.menu_width  = dpi(200)
theme.menu_border_width = dpi(0)
theme.menu_bg_normal = theme.color_dark
theme.menu_border_color = theme.color_dark

-- notification_[bg|fg|font|width|height|margin|border_color|border_width|shape|opacity]
theme.notification_fg       = theme.color01
theme.notification_bg       = theme.color_dark
theme.notification_opacity  = 0.90

-- prompt_[fg|bg|fg_cursor|bg_cursor|font]
theme.prompt_fg     = theme.color01
theme.prompt_bg     = theme.color_dark

-- hotkeys_[bg|fg|border_width|border_color|shape|opacity|modifiers_fg|label_bg|label_fg|group_margin|font|description_font]
theme.hotkeys_fg = theme.color01
theme.hotkeys_bg = theme.color_dark
theme.hotkeys_modifiers_fg = theme.color02
theme.hotkeys_opacity = 0.90
theme.hotkeys_border_width = 1
theme.hotkeys_border_color = theme.color01
theme.hotkeys_group_margin = 40

-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]

-- Generate taglist squares:
local taglist_square_size = dpi(2)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
    taglist_square_size, theme.color00
)
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
    taglist_square_size, theme.color00
)

-- Titlebar buttons
theme.titlebar_close_button_normal = theme_path.."titlebar/close_normal.png"
theme.titlebar_close_button_focus  = theme_path.."titlebar/close_focus.png"

theme.titlebar_minimize_button_normal = theme_path.."titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus  = theme_path.."titlebar/minimize_focus.png"

theme.titlebar_ontop_button_normal_inactive = theme_path.."titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive  = theme_path.."titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = theme_path.."titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active  = theme_path.."titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive = theme_path.."titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive  = theme_path.."titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = theme_path.."titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active  = theme_path.."titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive = theme_path.."titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive  = theme_path.."titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = theme_path.."titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active  = theme_path.."titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = theme_path.."titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = theme_path.."titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active = theme_path.."titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active  = theme_path.."titlebar/maximized_focus_active.png"

-- Layouts icons
theme.layout_fairh = default_path.."default/layouts/fairhw.png"
theme.layout_fairv = default_path.."default/layouts/fairvw.png"
theme.layout_floating  = default_path.."default/layouts/floatingw.png"
theme.layout_magnifier = default_path.."default/layouts/magnifierw.png"
theme.layout_max = default_path.."default/layouts/maxw.png"
theme.layout_fullscreen = default_path.."default/layouts/fullscreenw.png"
theme.layout_tilebottom = default_path.."default/layouts/tilebottomw.png"
theme.layout_tileleft   = default_path.."default/layouts/tileleftw.png"
theme.layout_tile = default_path.."default/layouts/tilew.png"
theme.layout_tiletop = default_path.."default/layouts/tiletopw.png"
theme.layout_spiral  = default_path.."default/layouts/spiralw.png"
theme.layout_dwindle = default_path.."default/layouts/dwindlew.png"
theme.layout_cornernw = default_path.."default/layouts/cornernww.png"
theme.layout_cornerne = default_path.."default/layouts/cornernew.png"
theme.layout_cornersw = default_path.."default/layouts/cornersww.png"
theme.layout_cornerse = default_path.."default/layouts/cornersew.png"

-- Awesome menu icon
theme.awesome_icon = theme_assets.awesome_icon(
    theme.menu_height, theme.color01, theme.color_dark
)

-- Icon theme
theme.icon_theme = nil

return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
