local awful = require("awful")
local gears = require("gears")
local hotkeys_popup = require("awful.hotkeys_popup")
local menubar = require("menubar")

require("config.vars")
require("lib.libkeys")

local keyboard = {}

--[[--------------------------------------------------------
        INIT
--]]--------------------------------------------------------

function keyboard.init() globalkeys = gears.table.join(

        -- Apps
        bindApp(super, enter, terminal, "launcher", "open a terminal"),

        -- Show Help
        bind(super, "s", "awesome", "show help", hotkeys_popup.show_help),

        -- Main menu
        bind(super, "w", "awesome", "show main menu", function () mymainmenu:show() end ),

        -- Awesome session
        bind(super_ctrl, "r", "awesome", "reload awesome", awesome.restart),
        bind(super_shift, "q", "awesome", "quit awesome", awesome.quit),

        -- Next/Previous Desktop
        bind(super, left, "tag", "view previous", awful.tag.viewprev),
        bind(super, right, "tag", "view next", awful.tag.viewnext),

        -- Back to last desktop
        bind(super, esc, "tag", "go back", awful.tag.history.restore),

        -- Focus clients by index
        bind(super, "j", "client", "focus next by index",       function () awful.client.focus.byidx( 1) end ),
        bind(super, "k", "client", "focus previous by index",   function () awful.client.focus.byidx(-1) end ),

        -- Swap clients by index
        bind(super_shift, "j", "client", "swap with next client by index",      function () awful.client.swap.byidx( 1) end ),
        bind(super_shift, "k", "client", "swap with previous client by index",  function () awful.client.swap.byidx(-1) end ),

        -- Swap screen focus
        bind(super_ctrl, "j", "screen", "focus the next screen",        function () awful.screen.focus_relative( 1) end ),
        bind(super_ctrl, "k", "screen", "focus the previous screen",    function () awful.screen.focus_relative(-1) end ),

        -- Jump to urgent client
        bind(super, "u", "client", "jump to urgent client", awful.client.urgent.jumpto),

        -- Back to last client
        bind(super, tab, "client", "go back",
            function ()
                awful.client.focus.history.previous()
                if client.focus then
                    client.focus:raise()
                end
            end
        ),
        
        -- Master width factor
        bind(super, "l", "layout", "increase master width factor", function () awful.tag.incmwfact( 0.05) end ),
        bind(super, "h", "layout", "decrease master width factor", function () awful.tag.incmwfact(-0.05) end ),
        
        -- Master clients number
        bind(super_shift, "l", "layout", "decrease the number of master clients", function () awful.tag.incnmaster(-1, nil, true) end ),
        bind(super_shift, "h", "layout", "increase the number of master clients", function () awful.tag.incnmaster( 1, nil, true) end ),
        
        -- Layout columns
        bind(super_ctrl, "l", "layout", "decrease the number of columns", function () awful.tag.incncol(-1, nil, true) end ),
        bind(super_ctrl, "h", "layout", "increase the number of columns", function () awful.tag.incncol( 1, nil, true) end ),

        -- Select Next/Previous
        bind(super,         space, "layout", "select next",     function () awful.layout.inc( 1) end ),
        bind(super_shift,   space, "layout", "select previous", function () awful.layout.inc(-1) end ),

        -- Restore minimized
        bind(super_ctrl, "n", "client", "restore minimized", 
            function ()
                local c = awful.client.restore()
                -- Focus restored client
                if c then
                    c:emit_signal(
                        "request::activate", "key.unminimize", {raise = true}
                    )
                end
            end
        ),


        -- Prompt
        bind(super, "r", "launcher", "run prompt", function () awful.screen.focused().mypromptbox:run() end ),

        bind(super, "x", "awesome", "lua execute prompt",
            function ()
                awful.prompt.run {
                    prompt       = "Run Lua code: ",
                    textbox      = awful.screen.focused().mypromptbox.widget,
                    exe_callback = awful.util.eval,
                    history_path = awful.util.get_cache_dir() .. "/history_eval"
                }
            end
        ),
        
        -- Menubar
        bind(super, "p", "launcher", "show the menubar", function() menubar.show() end )

    )

    ----------------------
    --     Desktops     --
    ----------------------
    for i = 1, 9 do globalkeys = gears.table.join(globalkeys,

        -- View tag only.
        bind(super, "#" .. i + 9, "tag", "view tag #"..i,
            function ()
                local screen = awful.screen.focused()
                local tag = screen.tags[i]
                if tag then
                    tag:view_only()
                end
            end
        ),
        
        -- Toggle tag display.
        bind(super_ctrl, "#" .. i + 9, "tag", "toggle tag #" .. i,
            function ()
                local screen = awful.screen.focused()
                local tag = screen.tags[i]
                if tag then
                    awful.tag.viewtoggle(tag)
                end
            end
        ),

        -- Move client to tag.
        bind(super_shift, "#" .. i + 9, "tag", "move focused client to tag #"..i, 
            function ()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then
                        client.focus:move_to_tag(tag)
                    end
                end
            end
        ),

        -- Toggle tag on focused client.
        bind(super_ctrl_shift, "#" .. i + 9, "tag", "toggle focused client on tag #" .. i,
            function ()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then
                        client.focus:toggle_tag(tag)
                    end
                end
            end
        )

    )end

    root.keys(globalkeys)

end

--[[--------------------------------------------------------
        CLIENTKEYS
--]]--------------------------------------------------------

function keyboard.clientkeys() return gears.table.join(

        -- Client Close
        bind(super, "F4", "client", "close window", function (c) c:kill() end ),

        -- Move to Master
        bind(super_ctrl, enter, "client", "move to master", function (c) c:swap( awful.client.getmaster() ) end),
        
        -- Move to screen
        bind(super, "o", "client", "move to screen", function (c) c:move_to_screen() end),

        -- Client Fullscreen
        bind(super, "f", "client", "toggle fullscreen", function (c) c.fullscreen = not c.fullscreen c:raise() end),

        -- Client Floating
        bind(super_ctrl, space, "client", "toggle floating", awful.client.floating.toggle),
            
        -- Client on top
        bind(super, "t", "client", "toggle keep on top", function (c) c.ontop = not c.ontop end),

        -- Client Minimize
        bind(super, "n", "client", "minimize", function (c) c.minimized = true end),

        -- Client Maximize
        bind(super,         "m", "client", "(un)maximize",              function (c) c.maximized = not c.maximized c:raise() end),
        bind(super_ctrl,    "m", "client", "(un)maximize vertically",   function (c) c.maximized_vertical = not c.maximized_vertical c:raise() end),
        bind(super_shift,   "m", "client", "(un)maximize horizontally", function (c) c.maximized_horizontal = not c.maximized_horizontal c:raise() end)

)end

--[[--------------------------------------------------------
        EOF
--]]--------------------------------------------------------

return keyboard