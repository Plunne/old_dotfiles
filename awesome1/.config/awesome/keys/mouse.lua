local awful = require("awful")
local gears = require("gears")
local hotkeys_popup = require("awful.hotkeys_popup")

require("config.vars")
require("lib.libkeys")

local mouse = {}


--[[--------------------------------------------------------
        INIT
--]]--------------------------------------------------------

function mouse.init() root.buttons( gears.table.join(
        
    -- Awesome Menu
    click(right_click, function () mymainmenu:toggle() end )

))end

--[[--------------------------------------------------------
        CLIENTBUTTONS
--]]--------------------------------------------------------

function mouse.clientbuttons() return gears.table.join(

    -- Activate client
    click(left_click, function (c) c:emit_signal("request::activate", "mouse_click", {raise = true}) end ),

    -- Move client
    clickMod(super, left_click, function (c) c:emit_signal("request::activate", "mouse_click", {raise = true}) awful.mouse.client.move(c) end ),

    -- resize client
    clickMod(super, right_click, function (c) c:emit_signal("request::activate", "mouse_click", {raise = true}) awful.mouse.client.resize(c) end )

)end

--[[--------------------------------------------------------
        TAGLIST
--]]--------------------------------------------------------

function mouse.taglist_mouse() return gears.table.join(

    -- Switch/Toggle desktop
    click(left_click, function(t) t:view_only() end ),	
    click(right_click, awful.tag.viewtoggle),

    -- Next/Previous desktop
    click(scroll_up,    function(t) awful.tag.viewnext(t.screen) end ),
    click(scroll_down,  function(t) awful.tag.viewprev(t.screen) end ),

    -- Move the active window to the desktop
    clickMod(super, left_click, 
        function(t)			        
            if client.focus then
                client.focus:move_to_tag(t)
            end
        end
    ),

    -- Clone the active window to the desktop
    clickMod(super, right_click, 
        function(t)			    
            if client.focus then
                client.focus:toggle_tag(t)
            end
        end
    )

)end

--[[--------------------------------------------------------
        TASKLIST
--]]--------------------------------------------------------

function mouse.tasklist_mouse() return gears.table.join(

    -- Toggle window
    click(left_click,
        function (c)
            if c == client.focus then
                c.minimized = true
            else
                c:emit_signal(
                    "request::activate",
                    "tasklist",
                    {raise = true}
                )
            end
        end
    ),

    -- Tasklist menu
    click(right_click, function () awful.menu.client_list( { theme = { width = 250 } } ) end ),

    -- Next/Previous window
    click(scroll_up,    function () awful.client.focus.byidx( 1) end ),
    click(scroll_down,  function () awful.client.focus.byidx(-1) end )

)end



--[[--------------------------------------------------------
        EOF
--]]--------------------------------------------------------

return mouse