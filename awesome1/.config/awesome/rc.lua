-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

-- Autofocus
require("awful.autofocus")

-- Theme
require("beautiful").init("~/.config/awesome/theme.lua")

-- Awesome config files
require("system.errors")
require("keys")
require("modules.bar")(s)
require("system.signals")
require("config")