vim.o.termguicolors = true

color00 = "#6c65c0"
color01 = "#413c73"
color02 = "#2e2b52"
transparent = "#00000000"

-- colors for active , inactive buffer tabs 
require "bufferline".setup {
    options = {
        indicator_icon = "▎",
        buffer_close_icon = "",
        modified_icon = "",
        close_icon = "",
        left_trunc_marker = "",
        right_trunc_marker = "",
        max_name_length = 14,
        max_prefix_length = 13,
        tab_size = 18,
        enforce_regular_tabs = true,
        view = "multiwindow",
        show_buffer_close_icons = true,
        offsets = {{filetype = "NvimTree", text = "", text_align = "left"}},
        separator_style = "thin"
    },
    highlights = {

        -- Background
        background = {
            guifg = color01,
            guibg = transparent,
        },

        -- buffers
        buffer_selected = {
            guifg = color00,
            guibg = transparent,
            gui = "bold",
        },
        buffer_visible = {
            guifg = color00,
            guibg = transparent,
        },

        -- close buttons
        close_button = {
            guifg = color01,
            guibg = transparent,
        },
        close_button_visible = {
            guifg = color00,
            guibg = transparent,
        },
        close_button_selected = {
            guifg = color00,
            guibg = transparent,
        },
        fill = {
            guifg = transparent,
            guibg = transparent,
        },
        indicator_selected = {
            guifg = color00,
            guibg = transparent,
        },

        -- modified
        modified = {
            guifg = color01,
            guibg = transparent,
        },
        modified_visible = {
            guifg = color00,
            guibg = transparent,
        },
        modified_selected = {
            guifg = color00,
            guibg = transparent,
        },

        -- separators
        separator = {
            guifg = color01,
            guibg = transparent,
        },
        separator_visible = {
            guifg = color00,
            guibg = transparent,
        },
        separator_selected = {
            guifg = color01,
            guibg = transparent,
        },
        -- tabs
        tab = {
            guifg = color01,
            guibg = transparent,
        },
        tab_selected = {
            guifg = color00,
            guibg = transparent,
        },
        tab_close = {
            guifg = color00,
            guibg = transparent,
        } 
    }
}

local opt = {silent = true}

vim.g.mapleader = " "

--command that adds new buffer and moves to it
vim.api.nvim_command "com -nargs=? -complete=file_in_path New badd <args> | blast"
vim.api.nvim_set_keymap("n","<S-b>",":New ", opt)

--removing a buffer
vim.api.nvim_set_keymap("n","<S-f>",[[<Cmd>bdelete<CR>]], opt)

-- tabnew and tabprev
vim.api.nvim_set_keymap("n", "<S-l>", [[<Cmd>BufferLineCycleNext<CR>]], opt)
vim.api.nvim_set_keymap("n", "<S-s>", [[<Cmd>BufferLineCyclePrev<CR>]], opt)
