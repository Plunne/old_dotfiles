local cmd = vim.cmd
local g = vim.g

local scopes = {o = vim.o, b = vim.bo, w = vim.wo}

local function opt(scope, key, value)
    scopes[scope][key] = value
    if scope ~= "o" then
        scopes["o"][key] = value
    end
end

---------------------
--     Settings    --
---------------------

-- cmd
cmd("syntax enable")
cmd("syntax on")
cmd("set noshowmode")
cmd("set cursorline")
-- opt
opt("o", "background", "light")
opt("o", "encoding", "utf-8")
opt("o", "fileencoding", "utf-8")
opt("o", "hidden", true)
opt("o", "ignorecase", true)
opt("o", "splitbelow", true)
opt("o", "splitright", true)
opt("o", "termguicolors", true)
opt("w", "number", true)
opt("o", "numberwidth", 4)
opt("o", "mouse", "a")
opt("w", "signcolumn", "yes")
opt("o", "cmdheight", 1)
opt("o", "updatetime", 250)             -- update interval for gitsigns 
opt("o", "clipboard", "unnamedplus")
opt("b", "tabstop", 4)                  -- for indenline
opt("b", "expandtab", true )
opt("b", "shiftwidth", 4 )
opt("o", "smarttab", true)
opt("o", "autochdir", true)             -- auto dir
-- g
g.mapleader = " "
g.auto_save = 0
g.one_nvim_transparent_bg = true

-- blankline
local indent = 4
g.indentLine_enabled = 1
g.indent_blankline_char = "▎"
g.indent_blankline_filetype_exclude = {"help", "terminal"}
g.indent_blankline_show_trailing_blankline_indent = false
g.indent_blankline_show_first_indent_level = false
