local cmd = vim.cmd

cmd("colorscheme default")

local colors = {
        ['main'] = '#6c65c0',
        ['red'] = '#e06c75',
        ['wine'] = '#af0000',
        ['green'] = '#008700',
        ['olive'] = '#98c379',
        ['orange'] = '#c07a30',
        ['yellow'] = '#d7af5f',
        ['gold'] = '#af875f',
        ['blue'] = '#4090ff',
        ['ocean'] = '#005f87',
        ['marine'] = "#005faf",
        ['cherry'] = '#d70087',
        ['purple'] = '#6c65c0',
        ['cyan'] = '#00ffff',
        ['dark'] = '#211f3b',
        ['dark-grey'] = '#2e2b52',
        ['mid-grey'] = "#413c73",
        ['light-grey'] = "#6c65c0",
        ['light'] = '#abb2bf',
        [''] = 'NONE'
}

local italic = 'italic'
local bold = 'bold'
local reverse = 'reverse'
local underline = 'underline'
local none = 'none'

local function hi(key, fg, bg, style)
    cmd("hi ".. key .. " guifg=" .. colors[fg])
    cmd("hi ".. key .. " guibg=" .. colors[bg])
    cmd("hi ".. key .. " gui=" .. style)
end

-- General
hi('Normal', 'light', '', none)
hi('Comment', 'mid-grey', '', italic)
hi('Visual', 'dark', 'main', none)

-- Parentheses
hi('MatchParen', 'main', 'dark', underline)

-- Search
hi('Search', 'dark', 'yellow', none)
hi('IncSearch', 'dark', 'cyan', none)

-- Line Numbers
hi('LineNr', 'mid-grey', '', none)
hi('EndOfBuffer', 'dark-grey', '', none)
hi('SignColumn', 'mid-grey', '', none)
hi('VertSplit', 'dark-grey', '', none)
hi('CursorLine', '', '', none)
hi('CursorLineNr', 'main', '', bold)

-- Status Line
hi('StatusLine', '', '', none)
hi('StatusLineNC', '', '', none)

--  General Coding
local Value = 'orange'
hi('Constant', Value, '', none)
hi('String', 'olive', '', none)
hi('Number', Value, '', none)
hi('Boolean', Value, '', italic)
hi('Float', Value, '', none)

hi('Identifier', 'main', '', none)
hi('Function', 'blue', '', none)

local Statement = 'yellow'
hi('Statement', Statement, '', none)
hi('Conditional', Statement, '', bold)
hi('Repeat', Statement, '', bold)
hi('Label', Statement, '', bold)
hi('Operator', 'cherry', '', none)
hi('Keyword', Statement, '', none)
hi('Exception', Statement, '', bold)

local Preproc = 'purple'
hi('Preproc', 'purple', '', none)
hi('Include', Preproc, '', none)
hi('Define', Preproc, '', none)
hi('Macro', Preproc, '', none)
hi('PreCondit', Preproc, '', none)

local Type = 'cherry'
hi('Type', Type, '', italic)
hi('StorageClass', Type, '', bold)
hi('Structure', Type, '', italic)
hi('Typedef', Type, '', bold)

local Special = 'blue'
hi('Special', Special, '', none)
hi('SpecialChar', Special, '', none)
hi('Tag', Special, '', none)
hi('Delimiter', Special, '', none)
hi('SpecialComment', 'mid-grey', '', none)
hi('Debug', Special, '', none)

hi('Underlined', 'marine', '', underline)

hi('Ignore', 'light', '', bold)
hi('Error', 'red', '', bold)
hi('Todo', 'orange', '', bold)

hi('Title', 'light', '', bold)
hi('Global', 'light', '', bold)

-- Git
hi('DiffAdd', 'green', '', none)
hi('DiffDelete', 'red', '', none)
hi('DiffChange', 'cyan', '', none)
hi('DiffModified', 'cyan', '', none)

-- Pmenu
hi('Pmenu', 'light', 'dark-grey', none)
hi('PmenuSel', 'dark', 'main', bold)
hi('PmenuSbar', '', 'mid-grey', none)
hi('PmenuThumb', '', 'main', none)

---------------------
--     Plugins     --
---------------------

-- Indent Blankline
hi('IndentBlanklineChar', 'dark-grey', '', none)

-- NVim Tree
hi('NvimTreeNormal', '', '', none)
hi('NvimTreeIndentMarker', 'dark-grey', '', none)
hi('NvimTreeFolderIcon', 'main', '', none)
hi('NvimTreeFolderName', 'main', '', none)
hi('NvimTreeOpenedFolderName','main','', bold)
hi('NvimTreeStatusLine', '', '', none)
hi('NvimTreeStatusLineNC', '', '', none)

-----------------------
--     Languages     --
-----------------------

-- CSS
hi('cssProp', 'olive', '', none)
hi('cssTagName', 'cherry', '', bold)
hi('cssIdentifier', 'purple', '', italic)
hi('cssClassName', 'blue', '', none)
hi('cssBraces', 'light', '', none)

-- HTML
hi('htmlTag', 'light', '', none)
hi('htmlTagName', 'cherry', '', none)
hi('htmlEndTag', 'light', '', none)
hi('htmlArg', 'blue', '', none)

-- Ini
hi('dosiniHeader', 'cherry', '', bold)
hi('dosiniLabel', 'blue', '', none)

-- Json
hi('jsonKeyword', 'blue', '', none)
hi('jsonBraces', 'main', '', none)
hi('jsoncKeywordMatch', 'blue', '', none)
hi('jsoncBraces', 'cherry', '', none)

-- Lua
hi('luaFunction', 'cherry', '', none)
hi('luaFuncName', 'blue', '', bold)
hi('luaFuncCall', 'blue', '', none)

-- Makefile
hi('makeIdent', 'cherry', '', bold)
hi('makeSpecTarget', 'olive', '', none)
hi('makeTarget', 'blue', '', none)
hi('makeStatement', 'purple', '', none)
hi('makeCommands', 'orange','', none)
hi('makeSpecial', 'orange', '', none)

-- Php
hi('phpIdentifier', 'light', '', none)
hi('phpParent', 'light', '', none)
hi('phpSuperglobals', 'cherry', '', bold)

-- Python
hi('pythonImport', 'orange', '', bold)
hi('pythonStatement', 'purple', '', bold)
hi('pythonFunction', 'cyan', '', bold)

-- Shell
hi('bashAdminStatement', 'light', '', none)
hi('bashStatement', 'light', '', none)
hi('shCaseEsac', 'cherry', '', none)
hi('shCommandSub', 'light', '', none)
hi('shCmdSubRegion', 'light', '', none)
hi('shDerefSimple', 'light', '', bold)
hi('shFunctionKey', 'blue', '', bold)
hi('shOption', 'light', '', none)
hi('shRange', 'light', '', none)
hi('shSemicolon', 'light', '', none)
hi('shSet', 'light', '', none)
hi('shSnglCase', 'purple', '', bold)
hi('shStatement', 'blue', '', none)
hi('shVariable', 'light', '', bold)
hi('zshDelim', 'light', '', none)
hi('zshDeref', 'cherry', '', bold)
hi('zshFlag', 'light', '', none)
hi('zshVariableDef', 'light', '', bold)

-- Sxhkd
hi('sxModifier', 'blue', '', bold)
hi('sxKeysym', 'blue', '', none)
hi('sxHotkeySep', 'cherry', '', none)
hi('sxBrace', 'cherry', '', none)
hi('sxSequenceSep', 'cherry', '', none)

-- Yaml
hi('yamlBlockMappingKey', 'blue', '', none)
hi('yamlKeyValueDelimiter', 'cherry', '', none)
hi('yamlBlockCollectionItemStart', 'cherry', '', none)
