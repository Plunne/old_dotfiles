#!/bin/sh
#  _____  _                                     
# |  __ \| |                                    
# | |__) | |_   _ _ __  _ __   ___              
# |  ___/| | | | | '_ \| '_ \ / _ \             
# | |    | | |_| | | | | | | |  __/             
# |_|___ |_|\__,_|_| |_|_| |_|\___|       _     
# |_   _|         | |      | | |         | |    
#   | |  _ __  ___| |_ __ _| | |      ___| |__  
#   | | | '_ \/ __| __/ _` | | |     / __| '_ \ 
#  _| |_| | | \__ \ || (_| | | |  _  \__ \ | | |
# |_____|_| |_|___/\__\__,_|_|_| (_) |___/_| |_|
#
# creator : Lena aka Plunne
# more infos : https://gitlab.com/Plunne/plunne_dotfiles
#
# Everyone can use and edit this file #ShareYourCode <3
#________________________________________________________#

setup() {
    cp .apps ~
    cp Makefile ~
    cd ~
}

clean() {
	rm -rf ~/Makefile
}

countdown3() {
	echo "3" && sleep 1
	echo "2" && sleep 1
	echo "1" && sleep 1
}

reboot() {
    echo -e "\n/!\\ SYSTEM WILL REBOOT /!\\"
    countdown3
    reboot
}

###############
#     Yay     #
###############

yay() {
	echo -e "\n***** INSTALL YAY *****\n"
	countdown3
	pacman -S --needed git base-devel
    git clone https://aur.archlinux.org/yay.git
    cd yay
    makepkg -si
    cd ~
}

####################
#     Makefile     #
####################

makefile() {
	echo -e "\n***** MAKEFILE *****\n"
	countdown3
	sudo make all
}

###################
#     Install     #
###################

install_apps() {
    echo -e "\n***** INSTALL APPS *****\n"
	countdown3
    cd .apps
    ./apps.sh
    cd ~
}

#################
#     Miscs     #
#################

misc_fnkeys() {
	echo -e "\n***** MISC FN KEYS *****"
	countdown3
	sudo sh -c "echo options hid_apple fnmode=2 > /etc/modprobe.d/hid_apple.conf"
	sudo sh -c "sed -i 's/FILES=()/FILES=(\/etc\/modprobe.d\/hid_apple.conf)/g' /etc/mkinitcpio.conf"
	sudo mkinitcpio -p linux-lts
}

###############
#     ZSH     #
###############

zsh() {
	echo -e "\n***** INSTALL ZSH *****\n"
	countdown3
	sudo pacman -S zsh
	chsh -s /bin/zsh $USER
	sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
	git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
}

####################
#     Dotfiles     #
####################

dotfiles() {
	echo -e "\n***** INSTALL DOTS *****\n"
	countdown3
    	cd ~/.gitlab/plunne_dotfiles/awesomewm
	cp -rvpf . ~
	echo "dotfiles are copied"
    	cd ~
    	mkdir Documents Music Pictures Projects Videos
}

################
#     MAIN     #
################

echo "*************************"
echo "*     START INSTALL     *"
echo "*************************"

#setup
#yay
#makefile
#install_apps
misc_fnkeys
zsh
dotfiles
clean

echo "****************************"
echo "*     INSTALL FINISHED     *"
echo "****************************"

reboot
