# PLUNNE BSP4

<img width="65%" src="https://gitlab.com/Plunne/miscs/-/raw/master/img_dots/bsp4/bsp4-main-preview.png"/>
<br/>
<br/>

# Softwares I use

- **Terminal :** Alacritty
- **Editor :** Neovim (nightly 0.5)
- **Filemanager :** Ranger & Thunar
- **Web browser :** Firefox
- **PDF Reader :** Zathura
- **Screenshots :** Flameshot

# Environment

- **WM :** Bspwm
- **Bar :** Polybar
- **Keybinds :** Sxhkd
- **Composer :** Picom (Ibhagwan fork)
- **Launcher :** Rofi
- **Notifications :** Dunst
- **Wallpaper :** Feh

<br/>

# Features

There are some features you will have with my config.

### V-Stack Layout

<img width="49%" src="https://gitlab.com/Plunne/miscs/-/raw/master/img_dots/bsp4/bsp4-main-vstack.png"/>
<br/>

### Polybar

Full bar
<br/>
<img src="https://gitlab.com/Plunne/miscs/-/raw/master/img_dots/bsp4/bsp4-polybar-full.png"/>

Window Title (+Launcher)
<br/>
<img src="https://gitlab.com/Plunne/miscs/-/raw/master/img_dots/bsp4/bsp4-polybar-title.png"/>

Date & Time (2 modes)
<br/>
<img src="https://gitlab.com/Plunne/miscs/-/raw/master/img_dots/bsp4/bsp4-polybar-datetime.png"/>
<img src="https://gitlab.com/Plunne/miscs/-/raw/master/img_dots/bsp4/bsp4-polybar-time.png"/>

System Tray (+Powermenu)
<br/>
<img src="https://gitlab.com/Plunne/miscs/-/raw/master/img_dots/bsp4/bsp4-polybar-status.png"/>

Dock
<br/>
<img src="https://gitlab.com/Plunne/miscs/-/raw/master/img_dots/bsp4/bsp4-polybar-dock.png"/>

<br/>

### Zsh + Powerlevel10k + custom aliases

<img src="https://gitlab.com/Plunne/miscs/-/raw/master/img_dots/bsp4/bsp4-prompt-gitgreen.png"/>
<img src="https://gitlab.com/Plunne/miscs/-/raw/master/img_dots/bsp4/bsp4-prompt-gityellow.png"/>
<br/>

### Rofi

Launcher
<br/>
<img width="49%" src="https://gitlab.com/Plunne/miscs/-/raw/master/img_dots/bsp4/bsp4-rofi-launcher.png"/>

Power Menu
<br/>
<img width="49%" src="https://gitlab.com/Plunne/miscs/-/raw/master/img_dots/bsp4/bsp4-rofi-powermenu.png"/>

<br/>

# Colorscheme OnePlunne

### Alacritty (Ranger and more)

<img width="49%" src="https://gitlab.com/Plunne/miscs/-/raw/master/img_dots/bsp4/bsp4-alacritty-ranger.png"/>
<br/>

### Neovim (PaperPlunne Colorscheme)

<img width="49%" src="https://gitlab.com/Plunne/miscs/-/raw/master/img_dots/nvim_paperplunne/nvim-paperplunne-full.png"/>
<br/>

### Zathura

<img width="49%" src="https://gitlab.com/Plunne/miscs/-/raw/master/img_dots/bsp4/bsp4-zathura-main.png"/>
<img width="49%" src="https://gitlab.com/Plunne/miscs/-/raw/master/img_dots/bsp4/bsp4-zathura-index.png"/>
<br/>
<img src="https://gitlab.com/Plunne/miscs/-/raw/master/img_dots/bsp4/bsp4-zathura-completion.png"/>
<br/>

# Installation

1. Clone this repo and go to plunne_dotfiles/bspwm directory.
    ```shell
    git clone https://gitlab.com/Plunne/plunne_dotfiles
    cd plunne_dotfiles/bspwm
    ```

2. Check the `install.sh` and the `Makefile` and change some apps if you want. 

3. Run the shell script install.sh
    ```shell
    ./install.sh
    ```

4. The system will reboot automatically

5. Enjoy!

<br/>

# Thanks to

**siduck76 (for awesome nvim tips) :** https://github.com/siduck76/neovim-dotfiles <br/>
**adi1090x (for awesome rofi configs) :** https://github.com/adi1090x/rofi <br/>

Thanks you for watching/using my dots, have a nice moments with my config! <3
