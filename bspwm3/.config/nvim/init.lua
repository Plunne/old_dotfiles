-- load all plugins
require("plugins")
require("settings")
require("keymaps")
require("lua-devicons")
require("lua-bufferline")
require("lua-galaxyline")
require("lua-gitsigns")
require("lua-lsp")
require("lua-lspCompe")
require("lua-nvimTree")
require("lua-telescope")
require "colorizer".setup()

local cmd = vim.cmd
local g = vim.g

g.mapleader = " "
g.auto_save = 1

-- Color Scheme
cmd "colorscheme onedark"

-- Settings
cmd [[
set encoding=utf-8
set fileencoding=utf-8

syntax enable
syntax on

set noshowmode
]]

-- blankline

local indent = 4

g.indentLine_enabled = 1
g.indent_blankline_char = "|"

cmd("hi IndentBlanklineChar guifg=#373b43")

g.indent_blankline_filetype_exclude = {"help", "terminal"}
g.indent_blankline_show_trailing_blankline_indent = false
g.indent_blankline_show_first_indent_level = false


-- highlights
cmd("hi LineNr guibg=NONE")
cmd("hi SignColumn guibg=NONE")
cmd("hi VertSplit guibg=NONE")
cmd("hi DiffAdd guifg=#81A1C1 guibg = none")
cmd("hi DiffChange guifg =#3A3E44 guibg = none")
cmd("hi DiffModified guifg = #81A1C1 guibg = none")
cmd("hi EndOfBuffer guifg=#282c34")

cmd("hi TelescopeBorder   guifg=#3e4451")
cmd("hi TelescopePromptBorder   guifg=#3e4451")
cmd("hi TelescopeResultsBorder  guifg=#3e4451")
cmd("hi TelescopePreviewBorder  guifg=#525865")
cmd("hi PmenuSel  guibg=#98c379")

-- tree folder name , icon color
cmd("hi NvimTreeFolderIcon guifg = #ff87ff")
cmd("hi NvimTreeFolderName guifg = #ff87ff")
cmd("hi NvimTreeIndentMarker guifg=#525865")

cmd("hi Normal guibg=NONE ctermbg=NONE")

require("nvim-autopairs").setup()
require("lspkind").init(
    {
        File = " "
    }
)

-- nvimTree bg color
cmd("hi CustomExplorerBg guibg=#000b0c15")

vim.api.nvim_exec(
    [[
augroup NvimTree 
  au!
  au FileType NvimTree setlocal winhighlight=Normal:CustomExplorerBg
 augroup END
 ]],
    false
)
