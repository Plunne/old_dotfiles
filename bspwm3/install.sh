#!/bin/sh
#  _____  _                                     
# |  __ \| |                                    
# | |__) | |_   _ _ __  _ __   ___              
# |  ___/| | | | | '_ \| '_ \ / _ \             
# | |    | | |_| | | | | | | |  __/             
# |_|___ |_|\__,_|_| |_|_| |_|\___|       _     
# |_   _|         | |      | | |         | |    
#   | |  _ __  ___| |_ __ _| | |      ___| |__  
#   | | | '_ \/ __| __/ _` | | |     / __| '_ \ 
#  _| |_| | | \__ \ || (_| | | |  _  \__ \ | | |
# |_____|_| |_|___/\__\__,_|_|_| (_) |___/_| |_|
#
# creator : Lena aka Plunne
# more infos : https://gitlab.com/Plunne/plunne_dotfiles
#
# Everyone can use and edit this file #ShareYourCode <3
#________________________________________________________#

clean() {
	rm -rf ~/Makefile
	rm -rf ~/install.sh
}

countdown3() {
	echo "3" && sleep 1
	echo "2" && sleep 1
	echo "1" && sleep 1
}

###############
#     Yay     #
###############

install_yay() {
	echo -e "\n***** INSTALL YAY *****\n"
	countdown3
	pacman -Sy
	git clone https://aur.archlinux.org/yay.git
	cd yay
	makepkg -si
	cd ..
}

################
#     Apps     #
################

install_apps() {
	echo -e "\n***** INSTALL APPS *****\n"
	countdown3
	make all
}

###############
#     ZSH     #
###############

install_zsh() {
	echo -e "\n***** INSTALL ZSH *****\n"
	countdown3
	sudo pacman -S zsh
	echo -e "\n[zsh default]"
	chsh -s /bin/zsh $USER
	DOTS_PATH=$PWD
	cd ~
	sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
	git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
	cd $DOTS_PATH
}

#################
#     Miscs     #
#################

misc_fnkeys() {
	echo -e "\n***** MISC FN KEYS *****"
	countdown3
	sudo sh -c "echo options hid_apple fnmode=2 > /etc/modprobe.d/hid_apple.conf"
	sudo sh -c "sed -i 's/FILES=()/FILES=(\/etc\/modprobe.d\/hid_apple.conf)/g' /etc/mkinitcpio.conf"
	sudo mkinitcpio -p linux-lts
}

####################
#     Dotfiles     #
####################

install_dotfiles() {
	echo -e "\n***** INSTALL DOTS *****\n"
	countdown3
	cp -rvpf . ~
	clean
	echo "dotfiles are copied"
}

################
#     MAIN     #
################

echo "*************************"
echo "*     START INSTALL     *"
echo "*************************"

install_yay
install_apps
install_zsh
install_dotfiles
misc_fnkeys

echo "****************************"
echo "*     INSTALL FINISHED     *"
echo "****************************"

echo -e "\n/!\\ SYSTEM WILL REBOOT /!\\"
countdown3
reboot
