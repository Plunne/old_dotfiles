# plunne_dotfiles

<img width="65%" src="https://gitlab.com/Plunne/miscs/-/raw/master/img_dots/floating_bsp3.png"/>
<br/>
<br/>

# Softwares I use

- **Terminal :** Alacritty
- **Editor :** Neovim (nightly 0.5)
- **Filemanager :** Ranger
- **Web browser :** Firefox
- **PDF Reader :** Zathura
- **Screenshots :** Flameshot


# Environment

- **WM :** Bspwm
- **Bar :** Polybar
- **Keybinds :** Sxhkd
- **Composer :** Picom (Ibhagwan fork)
- **Launcher :** Rofi
- **Notifications :** dunst
- **Wallpaper :** Feh

<br/>

# Features

There are some features you will have with my config.

### Polybar

Full bar
<br/>
<img src="https://gitlab.com/Plunne/miscs/-/raw/master/img_dots/bsp3_pb_full.png"/>

Date menu
<br/>
<img src="https://gitlab.com/Plunne/miscs/-/raw/master/img_dots/bsp3_pb_date_open.png"/>

Power menu
<br/>
<img src="https://gitlab.com/Plunne/miscs/-/raw/master/img_dots/bsp3_pb_powermenu_open.png"/>
<br/>

### Automated corners

<img width="49%" src="https://gitlab.com/Plunne/miscs/-/raw/master/img_dots/fullscreen_bsp3.png"/>
<img width="49%" src="https://gitlab.com/Plunne/miscs/-/raw/master/img_dots/tiling_bsp3.png"/>
<br/>

### Zsh + Powerlevel10k + custom aliases

<img src="https://gitlab.com/Plunne/miscs/-/raw/master/img_dots/bsp3_zsh_p10k.png"/>
<br/>
<br/>

# Colorscheme OnePlunne

### Alacritty (Ranger and more)

<img width="49%" src="https://gitlab.com/Plunne/miscs/-/raw/master/img_dots/bsp3_alacritty_ranger.png"/>
<br/>

### Neovim (OneDark Colorscheme)

<img width="49%" src="https://gitlab.com/Plunne/miscs/-/raw/master/img_dots/bsp3_neovim_full.png"/>
<br/>

### Zathura (With transparency)

<img width="49%" src="https://gitlab.com/Plunne/miscs/-/raw/master/img_dots/bsp3_zathura_double.png"/>
<br/>

### More pictures soon...
<br/>
<br/>

# Installation

1. Clone this repo and go to plunne_dotfiles/bspwm directory.
    ```shell
    git clone https://gitlab.com/Plunne/plunne_dotfiles
    cd plunne_dotfiles/bspwm
    ```

2. Check the Makefile and change some apps if you want. 

3. Run the shell script install.sh
    ```shell
    ./install.sh
    ```

4. The system will reboot

5. Enjoy!

<br/>

# Thanks to

siduck76 (for awesome nvim tips) : https://github.com/siduck76/neovim-dotfiles

more is coming...

Thanks you for watching/using my dots, have a nice moments with my config! <3
